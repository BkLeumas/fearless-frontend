function createCard({name, description, location:{picture_url}}, starts, ends, location, hasDetails) {
    if (hasDetails === true)
    {
        return `
        <div class= "col">
            <div class="card shadow-lg mb-5 bg-white rounded ">
                <img src="${picture_url}" alt="" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${name}</h5>
                        <h6 class="card-subtitle text-primary">${location}</h6>
                        <p class="card-text">${description}</p>
                    </div>
                <div class="card-footer">${starts} - ${ends}</div>
            </div>
        </div>
`
    }
      else {
          return `
          <div class="card">
          <img src="..." class="card-img-top" alt="...">

          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>

        <div class="card" aria-hidden="true">
          <img src="..." class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title placeholder-glow">
              <span class="placeholder col-6"></span>
            </h5>
            <p class="card-text placeholder-glow">
              <span class="placeholder col-7"></span>
              <span class="placeholder col-4"></span>
              <span class="placeholder col-4"></span>
              <span class="placeholder col-6"></span>
              <span class="placeholder col-8"></span>
            </p>
            <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
          </div>
        </div>
      `;
      }
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            return response.json

        } else {
          const data = await response.json();

            let columnCounter = 0
          for (let conference of data.conferences) {
              // let hasDetails = false

              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                  let hasDetails = true
                  // ITS THIS LINE ^^^
                  const details = await detailResponse.json();

                //   const location = ;
                  const dateStart = new Date(details.conference.starts);
                  const starts = dateStart.toLocaleDateString();
                  const dateEnd = new Date(details.conference.ends);
                  const ends = dateEnd.toLocaleDateString();
                  const location = details.conference.location.name
                  const html = createCard(details.conference, starts, ends, location, hasDetails);
                  const column = document.getElementById('col-'+ columnCounter );
                  columnCounter += 1
                  if (columnCounter >=2 ){
                    columnCounter = 0
                  }
                  column.innerHTML += html;


                  const descriptionTag = document.querySelector('.card-text')
                  descriptionTag.innerHTML = details.conference.description

                  const imageTag = document.querySelector('.card-img-top')
                  imageTag.src = details.conference.location.picture_url;
                }
                else {
                    console.log("detail response not ok")

        }}

}
    } catch (e) {
        console.error(e);
        var mainFailed = document.getElementById('main');
        var failTrigger = `<div class="alert alert-success" role="alert">"mistakes were made"</div>`
        mainFailed.innerHTML += failTrigger

        }


});
